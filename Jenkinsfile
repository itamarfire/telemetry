def TAG = '${BRANCH_NAME}'
def E2E
def MTAG = '99-SNAPSHOT'
def BNUM
pipeline {
    agent any
    tools {
        maven 'maven_3.6.2'
    }
	stages {

		stage('Initialize'){
			stages{
				stage ('Initialize tags for release') {
					when {expression{ BRANCH_NAME ==~ /^release\/.*/ }}
		            steps {
		                script{
		                	sh("git fetch --tags")
							BNUM = sh(returnStdout: true,script:"echo \${BRANCH_NAME} | cut -d / -f2 | tr -d '\n'")  // example : 1.2
		                    TAG = sh(returnStdout: true,script: "git tag | grep ${BNUM} | sort -V | tail -1") // example : v.1.2.0
		                    echo "tag is ${TAG}"
		                    if (TAG) {
		                        def VER = sh(returnStdout: true,script:"echo '${TAG}' | cut -d . -f3").toInteger()
		                        def BNUM1 = sh(returnStdout: true,script:"echo '${TAG}' | cut -d . -f1")
		                        def BNUM2 = sh(returnStdout: true,script:"echo '${TAG}' | cut -d . -f2")
		                        VER++
		                        VER = VER.toString()
		                        TAG = sh(returnStdout: true, script:"echo '${BNUM1}.${BNUM2}.${VER}' | tr -d '\n'")

		                        MTAG = sh(returnStdout: true, script:"echo '${BNUM}.${VER}' | tr -d '\n'")
		                    }
		                    else{
		                    	TAG = sh(returnStdout: true, script:"echo 'v${BNUM}.0' | tr -d '\n'")
		                    	MTAG = sh(returnStdout: true, script:"echo '${BNUM}.0' | tr -d '\n'")
		                    }
		                    echo ("next tag is ${TAG}")
							echo ("next maven component release version is ${MTAG}")
		                }
						sh ("mvn versions:set -DnewVersion=${MTAG}")
		            }
		        }

				stage('Initialize feature'){
					when { expression { BRANCH_NAME ==~ /feature\/.*/ }}
					steps{
						script{
							E2E = sh(returnStdout: true, script: "git log -1 --pretty=%B | grep '#e2e' | wc -l").toInteger()
							if ( E2E > 0) {
								echo ("will execute end-to-end tests due to #e2e commit comment")						
							}							
						}
						
					}
				}
			}
		}
	    stage('Build'){
	    	when { 
	    		anyOf{
	    			expression{ BRANCH_NAME ==~ /^release\/.*/ }
	    			branch 'master'
	    			expression { BRANCH_NAME ==~ /feature\/.*/ }
	    		}
	    	}
			steps {sh("mvn -s settings.xml clean verify")}
		}
		stage('Test'){
	    	when { 
	    		anyOf{
	    			expression{ BRANCH_NAME ==~ /^feature\/.*/ && E2E > 0 }
	    			branch 'master'
	    		}
	    	}
			steps {
				sh('''
					path=http://artifactory:8081/artifactory/libs-snapshot-local/com/lidar/analytics/99-SNAPSHOT
					version=$(curl -u admin:AP3cacfE431RB6KSmGnwxFEqXPB -s $path/maven-metadata.xml | grep value | tail -n 1  | sed "s/.*<value>\\([^<]*\\)<\\/value>.*/\\1/")
					curl -u admin:AP3cacfE431RB6KSmGnwxFEqXPB -X GET  $path/analytics-${version}.jar -o analytics.jar

					path=http://artifactory:8081/artifactory/libs-snapshot-local/com/lidar/simulator/99-SNAPSHOT
					version=$(curl -u admin:AP3cacfE431RB6KSmGnwxFEqXPB -s $path/maven-metadata.xml | grep value | tail -n 1  | sed "s/.*<value>\\([^<]*\\)<\\/value>.*/\\1/")
					curl -u admin:AP3cacfE431RB6KSmGnwxFEqXPB -X GET  $path/simulator-${version}.jar -o simulator.jar
					''')

				sh("mv tests-sanity.txt tests.txt")
				sh("java -cp analytics.jar:simulator.jar:target/telemetry-${MTAG}.jar com.lidar.simulation.Simulator")
				echo ("all tests passed")
			}
    	}
    	stage ('Publish'){
    		when{
    			anyOf{
    				expression{ BRANCH_NAME ==~ /^release\/.*/ }
    				branch 'master'
    			}
    		}
    		steps{
    			sh("mvn deploy -DskipTests")
    		}
    	}
    	stage ('Release'){
			when {expression{ BRANCH_NAME ==~ /^release\/.*/ }}
    		steps {
				sh ("""
					git reset --hard HEAD
					git tag ${TAG}
					git push -u origin ${TAG}
				""")

			}
    	}
	}
}